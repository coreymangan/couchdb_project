import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

/**
 *
 * @export
 * @class ApiService
 */

@Injectable()
export class ApiService {

  /**
   *Creates an instance of ApiService.
   * @param {HttpClient} http
   * @memberof ApiService
   */

  constructor(private http: HttpClient) { }

  cachedRequests: Array<HttpRequest<any>> = [];

  /**
   *
   *
   * @param {string} path
   * @param {HttpParams} [params=new HttpParams()]
   * @returns {Observable<any>}
   * @memberof ApiService
   */

  public get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.api_url}${path}`, {
      params
    })
      .pipe(catchError(this.formatErrors));
  }

  /**
   *
   *
   * @param {string} path
   * @param {Object} body
   * @returns {Observable<any>}
   * @memberof ApiService
   */

  public post(path: string, body: Object): Observable<any> {
    return this.http.post(`${environment.api_url}${path}`, body)
      .pipe(catchError(this.formatErrors));
  }

  /**
   *
   *
   * @param {string} path
   * @param {Object} body
   * @returns {Observable<any>}
   * @memberof ApiService
   */

  public put(path: string, body: Object): Observable<any> {
    return this.http.put(`${environment.api_url}${path}`, JSON.stringify(body),
      { headers: { 'Content-Type': 'application/json' } })
      .pipe(catchError(this.formatErrors));
  }

  /**
   *
   *
   * @param {*} path
   * @returns {Observable<any>}
   * @memberof ApiService
   */

  public delete(path): Observable<any> {
    return this.http.delete(`${environment.api_url}${path}`).pipe(catchError(this.formatErrors));
  }

  /**
   *
   *
   * @private
   * @param {*} error
   * @returns
   * @memberof ApiService
   */

  private formatErrors(error: any) {
    return throwError(error);
  }

}
