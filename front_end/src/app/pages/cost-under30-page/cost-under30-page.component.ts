import { Component, OnInit } from '@angular/core';
import { GameService } from '../../shared/services/game.service';

@Component({
  selector: 'app-cost-under30-page',
  templateUrl: './cost-under30-page.component.html',
  styleUrls: ['./cost-under30-page.component.scss']
})
export class CostUnder30PageComponent implements OnInit {
  games: any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.getGames();
  }

  // Gets view of games under 20
  getGames() {
    this.gameService.getViewCostUnder30()
      .subscribe(res => {
        console.log(res);
        this.games = res.data;
      }, err => {
        console.log(err);
      });
  }

}
