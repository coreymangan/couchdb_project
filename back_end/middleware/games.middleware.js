const nano = require('nano')('http://localhost:5984/games');

// get game
const getGame = async function(req, res, next) {
    const id = req.params.game_id;
    
    nano.get(id, (err, body) => {
        if (err) {
            res.send({error: err});
        } else {
            next();
        }
    }, err => {
        res.send({error: err});
    });
}
module.exports.getGame = getGame;