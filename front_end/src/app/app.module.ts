import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MatButtonModule, MatFormFieldModule, MatInputModule, MatTableModule,
        MatSnackBarModule, MatGridListModule, MatCardModule } from '@angular/material';

import { ApiService } from './shared/services/api.service';
import { GameService } from './shared/services/game.service';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { HeaderComponent } from './shared/header/header/header.component';
import { CreateGamePageComponent } from './pages/create-game-page/create-game-page.component';
import { GameFormComponent } from './components/game-form/game-form.component';
import { ViewGamePageComponent } from './pages/view-game-page/view-game-page.component';
import { GameTableComponent } from './components/game-table/game-table.component';
import { UpdateGamePageComponent } from './pages/update-game-page/update-game-page.component';
import { GameViewTableComponent } from './components/game-view-table/game-view-table.component';
import { CostUnder30PageComponent } from './pages/cost-under30-page/cost-under30-page.component';
import { CostDifferenceOver20PageComponent } from './pages/cost-difference-over20-page/cost-difference-over20-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    CreateGamePageComponent,
    GameFormComponent,
    ViewGamePageComponent,
    GameTableComponent,
    UpdateGamePageComponent,
    GameViewTableComponent,
    CostUnder30PageComponent,
    CostDifferenceOver20PageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatSnackBarModule,
    MatGridListModule,
    MatCardModule
  ],
  providers: [ApiService, GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
