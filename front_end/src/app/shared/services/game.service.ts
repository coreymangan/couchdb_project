import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../../environments/environment';

/**
 *
 * @export
 * @class GameService
 */

@Injectable()
export class GameService {

    constructor(private apiService: ApiService) {

    }

    /**
     * Gets list of all games
     * @memberof GameService
     */
    public getGames() {
        return this.apiService.get(environment.endpoints.getGames);
    }

    /**
     * Gets a specified game
     * @param gameId id of game
     * @memberof GameService
     */
    public getGame(gameId: string) {
        return this.apiService.get(`${environment.endpoints.getGame}${gameId}`);
    }

    /**
     * Creates game
     * @param game game object
     * @memberof GameService
     */
    public createGame(game: object) {
        return this.apiService.post(environment.endpoints.createGame, game);
    }

    /**
     * Updates specified game
     * @param gameId id of game
     * @param gameRev rev id of game
     * @param game game object
     * @memberof GameService
     */
    public updateGame(gameId: string, gameRev: string, game: object) {
        return this.apiService.put(`${environment.endpoints.updateGame}${gameId}/${gameRev}`, game);
    }

    /**
     * Deletes specified game
     * @param gameId id of game
     * @param gameRev rev id of game
     * @memberof GameService
     */
    public removeGame(gameId: string, gameRev: string) {
        return this.apiService.delete(`${environment.endpoints.removeGame}${gameId}/${gameRev}`);
    }

    /**
     * Gets view of current cost under 30
     * @memberof GameService
     */
    public getViewCostUnder30() {
        return this.apiService.get(`${environment.endpoints.costUnder30}`);
    }

    /**
     * Gets view of difference between original cost and current cosr
     * @memberof GameService
     */
    public getViewCostDiffOf20() {
        return this.apiService.get(`${environment.endpoints.costDiff20}`);
    }

    /**
     * Gets map reduce for getting unique platforms that were mentioned in database
     * @memberof GameService
     */
    public getViewNumPlatforms() {
        return this.apiService.get(`${environment.endpoints.numPlatforms}`);
    }
}
