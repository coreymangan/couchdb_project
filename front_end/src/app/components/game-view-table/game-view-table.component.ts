import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-game-view-table',
  templateUrl: './game-view-table.component.html',
  styleUrls: ['./game-view-table.component.scss']
})
export class GameViewTableComponent implements OnInit {
  @Input()
  games: any;

  @Input()
  valueName: any;

  dataSource: MatTableDataSource<any>;
  displayedColumns: String[] = ['key', 'value'];


  constructor() {

  }

  ngOnInit() {
    console.log(this.games);
    this.dataSource = new MatTableDataSource(this.games.rows);
  }

  applyFilter(event: any) {
    console.log(event);
  }

}
