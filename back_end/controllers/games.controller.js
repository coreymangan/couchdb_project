// Games controller
const nano = require('nano')('http://localhost:5984/games');

/*callbacks are err, body, header*/

// Create document
const create = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;

    nano.insert(body).then(response => {
        console.log(response);
        res.send(response);
    })
}
module.exports.create = create;

// Get all documents
const getAll = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');

    nano.list({
        include_docs: true
    }, (err, body) => {
        console.log(body);
        res.send(body);
    }, err => {
        console.log(err);
        res.send(err);
    });
}
module.exports.getAll = getAll;

// Get one document
const get = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    const id = req.params.game_id;

    nano.get(id, (err, body) => {
        if (!err) {
            res.send(body);
        } else {
            res.send({
                error: err
            });
        }
    }, err => {
        res.send({
            error: err
        });
    });
}
module.exports.get = get;

// Update document
const update = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    const id = req.params.game_id;
    const rev = req.params.game_rev;
    const data = req.body;

    if (req.body.name) {
        nano.insert({
            _id: id,
            _rev: rev,
            name: data.name,
            genre: data.genre,
            platforms: data.platforms,
            original_cost: data.original_cost,
            current_cost: data.current_cost,
            release_date: data.release_date,
            publishers: data.publishers,
            developers: data.developers
        }, (err, body) => {
            if (!err) {
                res.send({
                    message: 'Game updated successfully',
                    body
                });
            } else {
                res.send({
                    error: err
                });
            }
        }, err => {
            res.send({
                error: err
            });
        });
    } else {
        res.send({
            error: 'Name is required'
        });
    }
}
module.exports.update = update;

// Delete document
const remove = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    const id = req.params.game_id;
    const rev = req.params.game_rev;

    nano.destroy(id, rev, (err, body) => {
        if (!err) {
            res.send({
                message: 'Game deleted successfully',
                body
            });
        } else {
            res.send({
                error: err
            });
        }
    }, err => {
        res.send({
            error: err
        });
    });
}
module.exports.remove = remove;

// Get games under 30 euro current_price
const currentCostUnder30 = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');

    nano.view('game', 'currentCostUnder30', (err, body) => {
        if (err) {
            console.log(err);
            res.send(err);
        }

        if (!body.rows.length) {
            res.send({rows: 0})
        } else {
            res.send({data: body});
        }
    });  
}
module.exports.currentCostUnder30 = currentCostUnder30;

// Get games that are more than 20 euro off
const costDifferenceOf20Plus = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');

    nano.view('game', 'costDifferenceOf20Plus', (err, body) => {
        if (err) {
            console.log(err);
            res.send(err);
        }

        if (!body.rows.length) {
            res.send({rows: 0})
        } else {
            res.send({data: body});
        }
    });  
}
module.exports.costDifferenceOf20Plus = costDifferenceOf20Plus;

// Get platforms that games are on from map reduce
const numPlatforms = async function (req, res) {
    res.setHeader('Content-Type', 'application/json');

    nano.view('game', 'numPlatforms', (err, body) => {
        if (err) {
            console.log(err);
            res.send(err);
        }

        if (!body.rows.length) {
            res.send({rows: 0})
        } else {
            res.send({data: body});
        }
    });  
}
module.exports.numPlatforms = numPlatforms;