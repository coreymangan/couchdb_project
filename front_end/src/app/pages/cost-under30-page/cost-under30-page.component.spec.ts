import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostUnder30PageComponent } from './cost-under30-page.component';

describe('CostUnder30PageComponent', () => {
  let component: CostUnder30PageComponent;
  let fixture: ComponentFixture<CostUnder30PageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostUnder30PageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostUnder30PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
