import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGamePageComponent } from './update-game-page.component';

describe('UpdateGamePageComponent', () => {
  let component: UpdateGamePageComponent;
  let fixture: ComponentFixture<UpdateGamePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGamePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGamePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
