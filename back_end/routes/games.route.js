const express = require('express');
const router = express.Router();

const gamesController = require('../controllers/games.controller');
const gamesMiddleware = require('../middleware/games.middleware');

// CRUD
router.post('/creategame', gamesController.create);
router.get('/games', gamesController.getAll);
router.get('/game/:game_id', gamesController.get);
router.put('/updategame/:game_id/:game_rev', gamesMiddleware.getGame, gamesController.update);
router.delete('/removegame/:game_id/:game_rev', gamesMiddleware.getGame, gamesController.remove);

// Views
router.get('/currentcostunder30', gamesController.currentCostUnder30);
router.get('/costdifferenceof20plus', gamesController.costDifferenceOf20Plus);
router.get('/platforms', gamesController.numPlatforms);

module.exports = router;