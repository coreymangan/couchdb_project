import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CreateGamePageComponent } from './pages/create-game-page/create-game-page.component';
import { ViewGamePageComponent } from './pages/view-game-page/view-game-page.component';
import { UpdateGamePageComponent } from './pages/update-game-page/update-game-page.component';
import { CostUnder30PageComponent } from './pages/cost-under30-page/cost-under30-page.component';
import { CostDifferenceOver20PageComponent } from './pages/cost-difference-over20-page/cost-difference-over20-page.component';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'creategame',
    component: CreateGamePageComponent
  },
  {
    path: 'viewgames',
    component: ViewGamePageComponent
  },
  {
    path: 'updategame/:gameId/:revId',
    component: UpdateGamePageComponent
  },
  {
    path: 'gamesunder30',
    component: CostUnder30PageComponent
  },
  {
    path: 'gamesover20off',
    component: CostDifferenceOver20PageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
