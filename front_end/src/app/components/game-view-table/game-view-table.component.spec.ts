import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameViewTableComponent } from './game-view-table.component';

describe('GameViewTableComponent', () => {
  let component: GameViewTableComponent;
  let fixture: ComponentFixture<GameViewTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameViewTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameViewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
