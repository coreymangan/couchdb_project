import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostDifferenceOver20PageComponent } from './cost-difference-over20-page.component';

describe('CostDifferenceOver20PageComponent', () => {
  let component: CostDifferenceOver20PageComponent;
  let fixture: ComponentFixture<CostDifferenceOver20PageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostDifferenceOver20PageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostDifferenceOver20PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
