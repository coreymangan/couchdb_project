import { Component, OnInit } from '@angular/core';
import { GameService } from '../../shared/services/game.service';
import { MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-game-table',
  templateUrl: './game-table.component.html',
  styleUrls: ['./game-table.component.scss']
})
export class GameTableComponent implements OnInit {
  total_rows: number;
  games: any;
  dataSource: MatTableDataSource<any>;
  displayedColumns: String[] = ['name', 'genre', 'platforms', 'developers', 'publishers', 'original_cost', 'current_cost',
                                'release_date', 'update', 'remove'];

  constructor(private gameService: GameService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getGames();
  }

  // Searches for game
  applyFilter(event: any) {
    console.log(event);
  }

  // Gets games from db
  getGames() {
    this.gameService.getGames()
    .subscribe(res => {
      this.total_rows = res.total_rows;
      this.games = res.rows.filter(x => x.id !== '_design/game');
      console.log(this.games);
      this.dataSource = new MatTableDataSource(this.games);
    }, err => {
      console.log(err);
      alert(err);
    });
  }

  // Removes game from db
  removeGame(gameId: string, revId: string) {
    this.gameService.removeGame(gameId, revId)
      .subscribe(res => {
        this.snackBar.open('Game Removed', 'Close', {
          duration: 500
        });
        this.getGames();
      }, err => {
        console.log(err);
        alert(err);
      });
  }

}
