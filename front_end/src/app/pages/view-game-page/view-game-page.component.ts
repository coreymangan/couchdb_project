import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/shared/services/game.service';

@Component({
  selector: 'app-view-game-page',
  templateUrl: './view-game-page.component.html',
  styleUrls: ['./view-game-page.component.scss']
})
export class ViewGamePageComponent implements OnInit {
  platforms: any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.getPlatformlist();
  }

  // Gets list of available platforms
  getPlatformlist() {
    this.gameService.getViewNumPlatforms()
      .subscribe(res => {
        console.log(res.data.rows[0].value);
        this.platforms = res.data.rows[0].value;
      });
  }

}
