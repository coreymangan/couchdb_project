import { Component, OnInit } from '@angular/core';
import { GameService } from '../../shared/services/game.service';

@Component({
  selector: 'app-cost-difference-over20-page',
  templateUrl: './cost-difference-over20-page.component.html',
  styleUrls: ['./cost-difference-over20-page.component.scss']
})
export class CostDifferenceOver20PageComponent implements OnInit {
  games: any;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.getGames();
  }

  // Gets view of games with price difference of 30
  getGames() {
    this.gameService.getViewCostDiffOf20()
      .subscribe(res => {
        this.games = res.data;
      }, err => {
        console.log(err);
      });
  }

}
