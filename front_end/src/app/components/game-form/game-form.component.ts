import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { GameService } from '../../shared/services/game.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.scss']
})
export class GameFormComponent implements OnInit {
  gameId: string;
  revId: string;
  game: any;
  gameForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private gameService: GameService, private route: ActivatedRoute,
              private router: Router) {
    // Getting Ids from url params
    this.route.params.subscribe(res => {
      this.gameId = res.gameId;
      this.revId = res.revId;
    });

    // Create Game Form
      this.gameForm = this.formBuilder.group({
        'name': ['', Validators.required],
        'genre': [''],
        'release_date': [''],
        'platforms': [''],
        'original_cost': [''],
        'current_cost': [''],
        'publishers': [''],
        'developers': ['']
      });
   }

  ngOnInit() {
    if (this.gameId) {
      this.getGame();
    }
  }

  // Get Game
  getGame() {
    this.gameService.getGame(this.gameId)
      .subscribe(res => {
        console.log(res);
        this.game = res;

        // Update Game Form
        this.gameForm = this.formBuilder.group({
          'name': [this.game.name, Validators.required],
          'genre': [this.game.genre],
          'release_date': [this.game.release_date],
          'platforms': ['' + this.game.platforms],
          'original_cost': [this.game.original_cost],
          'current_cost': [this.game.current_cost],
          'publishers': ['' + this.game.publishers],
          'developers': ['' + this.game.developers]
        });
      }, err => {
        console.log(err);
        alert(err);
      });
  }

  // Create Game
  createGame() {
    const game = {
      name: this.gameForm.value.name,
      genre: this.gameForm.value.genre,
      release_date: this.gameForm.value.release_date,
      platforms: this.gameForm.value.platforms.split(',').map(x => x.trim()),
      original_cost: this.gameForm.value.original_cost,
      current_cost: this.gameForm.value.current_cost,
      publishers: this.gameForm.value.publishers.split(',').map(x => x.trim()),
      developers: this.gameForm.value.developers.split(',').map(x => x.trim())
    };

    this.gameService.createGame(game)
      .subscribe(res => {
        console.log(res);
        alert('Game Created Successfully');
        this.router.navigate(['/viewgames']);
      }, err => {
        console.log(err);
        alert(err);
      });
  }

  // Update Game
  updateGame() {
    const game = {
      name: this.gameForm.value.name,
      genre: this.gameForm.value.genre,
      release_date: this.gameForm.value.release_date,
      platforms: this.gameForm.value.platforms.split(',').map(x => x.trim()),
      original_cost: this.gameForm.value.original_cost,
      current_cost: this.gameForm.value.current_cost,
      publishers: this.gameForm.value.publishers.split(',').map(x => x.trim()),
      developers: this.gameForm.value.developers.split(',').map(x => x.trim())
    };

    this.gameService.updateGame(this.gameId, this.revId, game)
      .subscribe(res => {
        console.log(res);
        alert('Game Updated Successfully');
        this.router.navigate(['/viewgames']);
      }, err => {
        console.log(err);
        alert(err);
      });
  }

}
