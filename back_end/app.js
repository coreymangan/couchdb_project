// app.js

const nano = require('nano')('http://localhost:5984');
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const cors =  require('cors');
const morgan = require('morgan');
const port = '3000';
const gameRoute = require('./routes/games.route');

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(morgan('tiny'));
app.use(cors());

// Checks if db is there, if not then create it
nano.db.list().then((body) => {
    if (!body) {
        nano.db.create('games').then((body) => {
            console.log('games database created');
        });
    }
});

// use db games
nano.use('games');

// use routes with prefix of /api
app.use('/api', gameRoute);

// Listen on port
app.listen(port, () => {
    console.log(`listening to port ${port}`);
});